from django.shortcuts import render, get_object_or_404

from .forms import ActivityForm, AttendeeForm
from .models import Activity, Attendee

# Create your views here.

def activity_view(request):
    activities = Activity.objects.all()
    context = {
        'activities' : activities,
    }
    return render(request, 'activity_view.html', context)

def attendee_create_view(request, name):
    form = AttendeeForm(request.POST or None)
    if form.is_valid():
        x = form.save(commit=False)
        x.activity_name = Activity.objects.get(title = name)
        x.save()
        form = AttendeeForm()

    context = {
        'form' : form,
    }
    return render(request, 'attendee_create.html', context)

def activity_create_view(request):
    form = ActivityForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = ActivityForm()

    context = {
        'form':form,
    }
    return render(request, 'activity_create.html', context )

def activity_delete_view(request,nama=None):
    activities = Activity.objects.all()
    obj = get_object_or_404(Attendee, nickname=nama)
    if request.method == 'POST':
        obj.delete()

    context = {
        'activities':activities,
    }
    return render(request, 'activity_view.html', context)