from django.db import models

# Create your models here.
class Activity(models.Model):
    title = models.CharField(max_length=30)

    def __str__(self):
        return self.title

class Attendee(models.Model):
    activity_name = models.ForeignKey(Activity, on_delete= models.CASCADE,null=True, blank=True)
    nickname = models.CharField(max_length=20, unique=True)
    fullname = models.CharField(max_length=60)
    id_line = models.CharField(max_length=20)

    def __str__(self):
        return self.nickname
