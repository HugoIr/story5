from django.urls import path

from . import views

urlpatterns = [
    path('', views.matkul_create_view),
    path('detail/', views.matkul_list_view),
    path('delete/<str:nama>', views.matkul_delete_view, name="delete"),
    path('detail/<str:nama>', views.matkul_detail_view, name="detail"),
]