from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib import messages
import datetime
from .forms import MatkulForm
from .models import Matkul, Tugas
from django.db.models import Q
from datetime import timedelta

def matkul_create_view(request):
    form = MatkulForm(request.POST or None)

    if form.is_valid():
        form.save()
        form = MatkulForm()

    context = {
        'form' : form,
    }
    return render(request, "matkul_create.html", context)

def matkul_list_view(request):
    obj = Matkul.objects.all()
    
    context = {
        'object' : obj
    }
    return render(request, "matkul_list.html", context)

def matkul_delete_view(request,nama=None):
    temp = get_object_or_404(Matkul, nama_matkul=nama)
    obj = get_object_or_404(Matkul, nama_matkul=nama)
    if request.method == 'POST' or 'GET':
        obj.delete()

    context = {
        "temp" : temp ,
        "object" : obj,

    }
    return render(request, 'matkul_delete.html', context)

def matkul_detail_view(request, nama):
    obj = Matkul.objects.get(nama_matkul = nama)
    now = datetime.datetime.now()
    tugasnya = Tugas.objects.filter(matkul = obj).order_by('deadline').reverse()
    tgs_gte_deadline = Tugas.objects.filter(matkul = obj).order_by('deadline').reverse().filter(Q(deadline__gte=now)|Q(deadline=None))
    tgs_lt_deadline = Tugas.objects.filter(matkul = obj).order_by('deadline').reverse().filter(Q(deadline__lt=now))

    context = {
        'object': obj,
        'tugasnya' : tugasnya,
        'tugas_gte_deadline' : tgs_gte_deadline,
        'tugas_lt_deadline' : tgs_lt_deadline,
        'now' : now,
        
    }
    return render(request, 'matkul_detail.html', context)
