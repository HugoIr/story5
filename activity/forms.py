from django import forms

from .models import Activity, Attendee

class ActivityForm(forms.ModelForm):

    class Meta:
        model = Activity
        fields = [
            'title',
        ]

class AttendeeForm(forms.ModelForm):

    class Meta:
        model = Attendee
        fields = [
            'activity_name',
            'nickname',
            'fullname',
            'id_line',

        ]
