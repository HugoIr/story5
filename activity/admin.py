from django.contrib import admin

from .models import Activity, Attendee
# Register your models here.

admin.site.register(Activity)

admin.site.register(Attendee)
