from django.urls import path

from . import views

urlpatterns = [
    path('', views.activity_view, name='activity'),
    path('create/attendee/<str:name>', views.attendee_create_view, name="add_attendee"),
    path('create/activity/', views.activity_create_view, name="create_activity"),
    path('delete/<str:nama>', views.activity_delete_view, name="delete"),
]