from django.contrib import admin

from .models import Matkul, Tugas

# Register your models here.
admin.site.register(Matkul)

admin.site.register(Tugas)