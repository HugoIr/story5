from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.index),
    path('home/', views.index),
    path('about/', views.about),
    path('time/', views.time),
    path('time/<int:id>', views.time_mod),


]