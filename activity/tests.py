from django.utils import timezone
from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import activity_view, attendee_create_view, activity_create_view, activity_delete_view
from .models import Activity, Attendee
from .forms import ActivityForm, AttendeeForm
from django.shortcuts import get_object_or_404
class TestUrl(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/activity/')
        self.assertEqual(response.status_code,200)

    def test_using_promotions_template(self):
        response = Client().get('/activity/')
        self.assertTemplateUsed(response, 'activity_view.html')

    def test_using_promotions_view_func(self):
        found = resolve('/activity/')
        self.assertEqual(found.func, activity_view)

class TestModel(TestCase):

    def setUp(self):
        #Creating a new coupon
        activity = Activity.objects.create(title='Berenang')
        Attendee.objects.create(activity_name=activity, nickname='hugo',fullname='hugo mantappu', id_line='adadeh')
        Attendee.objects.create(activity_name=activity, nickname='hugi',fullname='hugi mantippu', id_line='adadeh2')

    def test_model_can_create_new_objects(self):
        #Retrieving all available coupon
        counting_all_available_activity = Activity.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

    def test_str_is_equal_to_title(self):

        activity1 = Activity.objects.get(title='Berenang')
        attendee1 = Attendee.objects.get(nickname='hugo')
        attendee2 = Attendee.objects.get(nickname='hugi')
        self.assertEqual(str(activity1),activity1.title)
        self.assertEqual(str(attendee1), attendee1.nickname)
        self.assertEqual(str(attendee2), attendee2.nickname)

class TestView(TestCase):
    def setUp(self):
        #Creating a new coupon
        self.activity = Activity.objects.create(title='Berenang')
        Attendee.objects.create(activity_name=self.activity, nickname='hugo',fullname='hugo mantappu', id_line='adadeh')
        Attendee.objects.create(activity_name=self.activity, nickname='hugi',fullname='hugi mantippu', id_line='adadeh2')

    def test_attendee_create_view(self):
        
        form_data = {'activity_name':'',
            'nickname':'hago',
            'fullname':'hagoonlen',
            'id_line':'hahahago',
}
        form = AttendeeForm(data=form_data)
        # self.assertEqual(form_data['activity_name'],'Berenang')
        self.assertEqual(form_data['nickname'],'hago')
        form.save()
        self.assertTrue(form.is_valid())


    def test_delete_attendee(self):
        at = Attendee.objects.create(activity_name=self.activity, nickname='hugoz',fullname='hugo mantappu', id_line='adadeh')
        x = get_object_or_404(Attendee, nickname = 'hugoz')
        
        x.delete()