from django.db import models
import datetime as dt
from datetime import timedelta

# Create your models here.
SEMESTER = (('GASAL', 'Gasal'),
            ('GENAP 2', 'Genap'),
            )
TAHUN = (('2019/2020','2019/2020'),
         ('2020/2021','2020/2021'),
         ('2021/2022','2021/2022'),
         ('2022/2023','2022/2023'),
         ('2023/2024','2023/2024'),
         ('2024/2025','2024/2025'),

)


class Matkul(models.Model):
    nama_matkul = models.CharField(max_length=100, unique=True)
    dosen_pengajar = models.CharField(max_length=120)
    jumlah_sks = models.PositiveSmallIntegerField()
    deskripsi_matkul = models.TextField(blank=True, null=True)
    semester = models.CharField(max_length=10,choices=SEMESTER, default='Genap')
    tahun_ajaran = models.CharField(max_length=10,choices=TAHUN, default= '2019/2020')
    ruang_kelas = models.CharField(max_length=30)

    def __str__(self):
        return self.nama_matkul

class Tugas(models.Model):
    matkul = models.ForeignKey(Matkul, on_delete=models.CASCADE)
    tugas = models.TextField(max_length=120)
    deadline = models.DateTimeField()

    def __str__(self):
        return self.tugas